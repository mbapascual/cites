import React, {Fragment, useState} from 'react';

const Form = () => {

    // Definicion de States
    const [cita, guardarCita] = useState({
        mascota: '',
        propietario: '',
        fecha: '',
        hora: '',
        sintomas: ''
    });

    const [error, actualizarError] = useState(false);

    // Guardar valores en el State
    const handlerChange = (e) => {

        var campo = e.target.name + ": " + e.target.value;
        console.log(campo);

        guardarCita({
            ...cita,
            [e.target.name]: e.target.value
        })
    };

    // Extraer valores del State
    const {mascota, propietario, fecha, hora, sintomas} = cita;

    // Envio formulario
    const submitCita = (e) => {

        // Para que no haga el envio del formulario
        e.preventDefault();

        // Validar
        if (mascota.trim() === '') {
            actualizarError(true);
            return;
        }

        if (propietario.trim() === '') {
            actualizarError(true);
            return;
        }

        if (fecha.trim() === '') {
            actualizarError(true);
            return;
        }

        if (hora.trim() === '')
        {
            actualizarError(true);
            return;
        }

        if (sintomas.trim() === '')
        {
            actualizarError(true);
            return;
        }

        actualizarError(false);

        // Asiganr ID
        cita.id = fecha.trim() + hora.trim();

        // Crear Cita

        // Reiniciar Form
    };

    return (
        
        <Fragment>

            {error ? <p className="alerta-error">Todos los campos son obligatorios</p>  : null}

        <form
            name="Citas"
            method="POST"
            onSubmit={submitCita}
        >
            <label>Nombre mascota</label>
            <input
                type="text"
                name="mascota"
                value={mascota}
                className="u-full-width"
                placeholder="Nombre mascota"
                onChange={handlerChange}
            />
            <label>Nombre del propietario</label>
            <input
                type="text"
                name="propietario"
                value={propietario}
                className="u-full-width"
                placeholder="Propietario de la mascota"
                onChange={handlerChange}
            />
            <label>Fecha de alta</label>
            <input
                type="date"
                name="fecha"
                value={fecha}
                className="u-full-width"
                onChange={handlerChange}
            />
            <label>Hora de alta</label>
            <input
                type="time"
                name="hora"
                value={hora}
                className="u-full-width"
                onChange={handlerChange}
            />
            <label>Síntomas</label>
            <textarea
                name="sintomas"
                value={sintomas}
                className="u-full-width"
                onChange={handlerChange}
            ></textarea>
            <button
                type="submit"
                className="u-full-width button-primary"
            >Agregar Cita</button>
        </form>

        </Fragment>
    );
}

export default Form;